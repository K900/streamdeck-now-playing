{
  description = "Here be heinous crimes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    cross = {
      url = "gitlab:K900/rust-cross-windows";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      cross,
      pre-commit-hooks,
      ...
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {
      packages.x86_64-linux = rec {
        plugin = cross.lib.mkCrossPackage {
          src = ./.;
        };

        default = plugin;
      };

      checks.x86_64-linux.pre-commit-check = pre-commit-hooks.lib.x86_64-linux.run {
        src = ./.;

        tools = {
          cargo = cross.lib.crossToolchain;
          clippy = cross.lib.crossToolchain;
        };

        hooks = {
          nixfmt-rfc-style.enable = true;
          deadnix.enable = true;
          statix.enable = true;

          rustfmt.enable = true;
          clippy.enable = true;
          cargo-check.enable = true;
        };
      };

      devShells.x86_64-linux.default = cross.lib.mkCrossShell {
        inputsFrom = [ self.packages.x86_64-linux.default ];
        buildInputs = [
          pkgs.cargo-outdated
          pkgs.rustfmt
          pkgs.just
          pkgs.zip
        ];

        inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
      };
    };
}
