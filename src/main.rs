use std::{collections::HashSet, env};

use anyhow::{anyhow, bail};
use futures::{channel::mpsc::channel, SinkExt, StreamExt};
use streamdeck_rs::{registration::RegistrationParams, MessageOut};
use tokio::select;
use tracing::{error, info, Level};
use tracing_appender::rolling::{RollingFileAppender, Rotation};

type Socket = streamdeck_rs::StreamDeckSocket<(), (), (), ()>;

mod controls;
mod updates;
mod utils;

use controls::MediaControlsManager;
use updates::MediaCache;
use utils::ResultExt;

async fn real_main() -> anyhow::Result<()> {
    let params = RegistrationParams::from_args(env::args())
        .map_err(|e| anyhow!("Failed to register: {:?}", e))?;

    let mut socket = Socket::connect(params.port, params.event, params.uuid)
        .await
        .map_err(|e| anyhow!("Failed to connect: {:?}", e))?;

    let (media_sink, mut media_stream) = channel::<updates::MediaUpdate>(1);

    let media_controls = MediaControlsManager::setup(media_sink)?;

    let mut action_ids = HashSet::<String>::new();

    let mut cached_state = MediaCache::default();

    loop {
        select! {
            sd_message = socket.next() => {
                use streamdeck_rs::Message::*;
                match sd_message {
                    Some(Ok(m)) => match m {
                        WillAppear { context, .. } => {
                            socket.send(MessageOut::SetFeedback {
                                context: context.clone(),
                                payload: cached_state.as_json()
                            }).await.log_and_ignore("Failed to update action");
                            action_ids.insert(context);
                        }
                        WillDisappear { context, .. } => {
                            action_ids.remove(&context);
                        }
                        TouchTap { .. } | DialUp { .. } => {
                            media_controls.play_pause().log_and_ignore("Failed to toggle play/pause");
                        }
                        DialRotate { payload, .. } => {
                            if payload.ticks > 0 {
                                media_controls.next_track().log_and_ignore("Failed to skip to next track");
                            } else {
                                media_controls.previous_track().log_and_ignore("Failed to skip to previous track");
                            }
                        }
                        _ => {
                            // do nothing
                        },
                    },
                    Some(Err(e)) => {
                        bail!("Socket error: {:?}", e)
                    },
                    None => break,
                }
            },
            media_message = media_stream.next() => {
                match media_message {
                    Some(m) => {
                        let payload = m.clone().into_json();
                        for context in &action_ids {
                            socket.send(MessageOut::SetFeedback {
                                context: context.clone(),
                                payload: payload.clone()
                            }).await.log_and_ignore("Failed to update action");
                        };
                        cached_state.update(m);
                    },
                    None => break,
                }
            }
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let file_appender = RollingFileAppender::builder()
        .rotation(Rotation::DAILY)
        .filename_prefix("sd-plugin-nowplaying")
        .filename_suffix("log")
        .max_log_files(2)
        .build(".")?;

    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
    tracing_subscriber::fmt()
        .with_ansi(false)
        .with_writer(non_blocking)
        .with_max_level(Level::DEBUG)
        .init();

    info!("Starting...");

    let result = real_main().await;

    if let Err(e) = &result {
        error!("Error: {:?}", e);
    }
    result
}
