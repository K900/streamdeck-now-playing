use std::io::Cursor;

use anyhow::Context;
use base64::Engine;
use image::{DynamicImage, GenericImage, ImageFormat, ImageReader};
use serde_json::{json, Map, Value};
use tracing::warn;
use windows::{
    Media::Control::GlobalSystemMediaTransportControlsSession,
    Storage::Streams::{DataReader, IRandomAccessStreamReference},
};

const DISPLAY_WIDTH: u32 = 200;
const DISPLAY_HEIGHT: u32 = 100;

#[derive(Clone)]
pub(crate) struct MediaUpdate {
    track: Option<Option<String>>,
    artist: Option<Option<String>>,
    progress: Option<Option<u8>>,
    image: Option<Option<String>>,
}

impl MediaUpdate {
    pub(crate) fn clear() -> Self {
        Self {
            track: Some(None),
            artist: Some(None),
            progress: Some(None),
            image: Some(None),
        }
    }

    pub(crate) fn into_json(self) -> Value {
        let mut m = Map::new();

        if let Some(a) = self.artist {
            m.insert("artist".into(), a.into());
        }

        if let Some(t) = self.track {
            m.insert("track".into(), t.into());
        }

        if let Some(p) = self.progress {
            m.insert(
                "progress".into(),
                json!({
                    "enabled": p.is_some(),
                    "value": p.unwrap_or(0),
                }),
            );
        }

        if let Some(i) = self.image {
            m.insert("image".into(), i.into());
        }

        Value::Object(m)
    }

    fn convert_thumbnail(stream: IRandomAccessStreamReference) -> anyhow::Result<String> {
        let data = stream
            .OpenReadAsync()
            .context("When opening image stream")?
            .get()
            .context("When getting result of image stream")?;

        let size = data.Size().context("When getting image size")? as usize;
        let reader = DataReader::CreateDataReader(
            &data
                .GetInputStreamAt(0)
                .context("When creating input stream")?,
        )
        .context("When creating data reader")?;

        let mut buffer = vec![0u8; size];

        let n = reader
            .LoadAsync(size as u32)
            .context("When loading image data to reader")?
            .get()? as usize;

        reader
            .ReadBytes(&mut buffer[0..n])
            .context("When reading image to buffer")?;

        let image = ImageReader::new(Cursor::new(buffer))
            .with_guessed_format()
            .context("When guessing format")?
            .decode()
            .context("When decoding image")?;

        let mut new_image =
            DynamicImage::new(DISPLAY_WIDTH, DISPLAY_HEIGHT, image::ColorType::Rgb8);
        let scaled_image = image.resize(
            DISPLAY_WIDTH,
            DISPLAY_HEIGHT,
            image::imageops::FilterType::Lanczos3,
        );

        let new_x = (new_image.width() - scaled_image.width()) / 2;
        let new_y = (new_image.height() - scaled_image.height()) / 2;

        new_image
            .copy_from(&scaled_image, new_x, new_y)
            .context("When blitting the initial image")?;
        new_image = new_image.blur(10.0);
        new_image
            .copy_from(&scaled_image, new_x, new_y)
            .context("When blitting the clean image")?;

        let mut new_buffer = Vec::new();
        new_image
            .write_to(&mut Cursor::new(&mut new_buffer), ImageFormat::Png)
            .context("When writing new image")?;

        let image_b64 = base64::prelude::BASE64_STANDARD.encode(new_buffer);

        Ok(format!("data:image/png;base64,{}", image_b64))
    }

    pub(crate) fn from_media_properties(
        session: &GlobalSystemMediaTransportControlsSession,
    ) -> anyhow::Result<Self> {
        let props = session
            .TryGetMediaPropertiesAsync()
            .context("When getting media properties")?
            .get()
            .context("When getting media properties result")?;

        let artist = props.Artist().ok().map(|a| a.to_string());
        let track = props.Title().ok().map(|t| t.to_string());

        let image = match props
            .Thumbnail()
            .map_err(|e| e.into())
            .and_then(Self::convert_thumbnail)
        {
            Ok(im) => Some(im),
            Err(e) => {
                warn!("Failed to get thumbnail: {:?}", e);
                None
            }
        };

        Ok(Self {
            artist: Some(artist),
            track: Some(track),
            progress: None,
            image: Some(image),
        })
    }

    pub(crate) fn from_timeline_properties(
        session: &GlobalSystemMediaTransportControlsSession,
    ) -> anyhow::Result<Self> {
        let props = session
            .GetTimelineProperties()
            .context("When getting timeline properties")?;

        let start = props.StartTime()?.Duration;
        let end = props.EndTime()?.Duration;
        let position = props.Position()?.Duration;

        let progress = if start != end {
            Some(((position - start) as f64 / (end - start) as f64 * 100.0) as u8)
        } else {
            None
        };

        Ok(Self {
            track: None,
            artist: None,
            progress: Some(progress),
            image: None,
        })
    }

    pub(crate) fn from_session(
        session: &GlobalSystemMediaTransportControlsSession,
    ) -> anyhow::Result<Self> {
        let mut update = Self::from_media_properties(session)?;
        update.progress = Self::from_timeline_properties(session)?.progress;
        Ok(update)
    }
}

#[derive(Default)]
pub(crate) struct MediaCache {
    track: Option<String>,
    artist: Option<String>,
    progress: Option<u8>,
    image: Option<String>,
}

impl MediaCache {
    pub(crate) fn as_json(&self) -> Value {
        json!({
            "artist": self.artist,
            "track": self.track,
            "progress": {
                "enabled": self.progress.is_some(),
                "value": self.progress.unwrap_or(0),
            },
            "image": self.image
        })
    }

    pub(crate) fn update(&mut self, update: MediaUpdate) {
        if let Some(a) = update.artist {
            self.artist = a;
        }

        if let Some(t) = update.track {
            self.track = t;
        }

        if let Some(p) = update.progress {
            self.progress = p;
        }

        if let Some(i) = update.image {
            self.image = i;
        }
    }
}
