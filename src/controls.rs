use std::sync::{Arc, Mutex};

use anyhow::Context;
use futures::channel::mpsc::Sender;
use tracing::{debug, warn};
use windows::{
    Foundation::{EventRegistrationToken, TypedEventHandler},
    Media::Control::{
        GlobalSystemMediaTransportControlsSession, GlobalSystemMediaTransportControlsSessionManager,
    },
};

use crate::{updates::MediaUpdate, utils::ResultExt};

pub(crate) struct MediaControlsManager {
    manager: GlobalSystemMediaTransportControlsSessionManager,
    state: Mutex<MediaControlsManagerState>,
}

struct MediaControlsManagerState {
    sink: Sender<MediaUpdate>,
    session: Option<GlobalSystemMediaTransportControlsSession>,
    media_cb: Option<EventRegistrationToken>,
    progress_cb: Option<EventRegistrationToken>,
}

impl MediaControlsManager {
    fn new_session(self: &Arc<Self>) {
        let mut state = self.state.lock().unwrap();

        if let Some(old_session) = state.session.take() {
            if let Some(mcb) = state.media_cb.take() {
                old_session
                    .RemoveMediaPropertiesChanged(mcb)
                    .log_and_ignore("Failed to unregister old media data callback");
            }
            if let Some(pcb) = state.progress_cb.take() {
                old_session
                    .RemoveTimelinePropertiesChanged(pcb)
                    .log_and_ignore("Failed to unregister old progress callback");
            }
        }

        if let Ok(current_session) = self.manager.GetCurrentSession() {
            match MediaUpdate::from_session(&current_session) {
                Ok(u) => state
                    .sink
                    .try_send(u)
                    .log_and_ignore("Failed to send media data from new session"),
                Err(e) => warn!("Failed to get media data from new session: {:?}", e),
            }

            let s2 = self.clone();
            let media_properties_changed =
                current_session.MediaPropertiesChanged(&TypedEventHandler::new(move |_, _| {
                    s2.media_properties_changed();
                    Ok(())
                }));
            match media_properties_changed {
                Ok(token) => state.media_cb = Some(token),
                Err(e) => warn!("Failed to set up media data callback: {:?}", e),
            }

            let s3 = self.clone();
            let timeline_properties_changed =
                current_session.TimelinePropertiesChanged(&TypedEventHandler::new(move |_, _| {
                    s3.timeline_properties_changed();
                    Ok(())
                }));
            match timeline_properties_changed {
                Ok(token) => state.progress_cb = Some(token),
                Err(e) => warn!("Failed to set up progress callback: {:?}", e),
            }

            state.session = Some(current_session);
        } else {
            state
                .sink
                .try_send(MediaUpdate::clear())
                .log_and_ignore("Failed to clear media data");
            state.session = None;
        }
    }

    fn media_properties_changed(&self) {
        let mut state = self.state.lock().unwrap();
        if let Some(s) = &state.session {
            match MediaUpdate::from_media_properties(s) {
                Ok(u) => state
                    .sink
                    .try_send(u)
                    .log_and_ignore("Failed to send new media info"),
                Err(e) => warn!("Failed to get media data from session: {:?}", e),
            }
        }
    }

    fn timeline_properties_changed(&self) {
        let mut state = self.state.lock().unwrap();
        if let Some(s) = &state.session {
            match MediaUpdate::from_timeline_properties(s) {
                Ok(u) => state
                    .sink
                    .try_send(u)
                    .log_and_ignore("Failed to send playback progress"),
                Err(e) => warn!("Failed to get progress data from session: {:?}", e),
            }
        }
    }

    pub(crate) fn setup(sink: Sender<MediaUpdate>) -> anyhow::Result<Arc<Self>> {
        let manager = GlobalSystemMediaTransportControlsSessionManager::RequestAsync()?.get()?;

        let s = Arc::new(Self {
            manager: manager.clone(),
            state: Mutex::new(MediaControlsManagerState {
                sink,
                session: None,
                media_cb: None,
                progress_cb: None,
            }),
        });

        debug!("Created session tracker");

        let s2 = s.clone();
        manager
            .CurrentSessionChanged(&TypedEventHandler::new(move |_, _| {
                debug!("Got new session callback!");
                s2.new_session();
                Ok(())
            }))
            .context("When registering new session callback")?;

        debug!("Parsing initial session");
        s.new_session();

        Ok(s.clone())
    }

    pub(crate) fn play_pause(&self) -> anyhow::Result<()> {
        let state = self.state.lock().unwrap();
        if let Some(s) = &state.session {
            s.TryTogglePlayPauseAsync()?.get()?;
        }

        Ok(())
    }

    pub(crate) fn next_track(&self) -> anyhow::Result<()> {
        let state = self.state.lock().unwrap();
        if let Some(s) = &state.session {
            s.TrySkipNextAsync()?.get()?;
        }

        Ok(())
    }

    pub(crate) fn previous_track(&self) -> anyhow::Result<()> {
        let state = self.state.lock().unwrap();
        if let Some(s) = &state.session {
            s.TrySkipPreviousAsync()?.get()?;
        }

        Ok(())
    }
}
