use std::fmt::Debug;

use tracing::warn;

pub(crate) trait ResultExt: Sized {
    fn log_and_ignore(self, context: &str);
}

impl<T, E> ResultExt for Result<T, E>
where
    E: Debug,
{
    fn log_and_ignore(self, context: &str) {
        if let Err(e) = self {
            warn!("{}: {:?}", context, e);
        }
    }
}
