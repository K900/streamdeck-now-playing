ID := "me.0upti.nowplaying2"
DIRNAME := ID + ".sdPlugin"
FILENAME := ID + ".streamDeckPlugin"

deploy: package
    wslview target/{{FILENAME}}

build:
    cargo build --release

package: build
    #!/usr/bin/env bash
    set -euxo pipefail
    cd target
    rm -rf {{DIRNAME}} {{FILENAME}}
    mkdir {{DIRNAME}}
    cp x86_64-pc-windows-msvc/release/*.exe {{DIRNAME}}
    sed s/@date@/$(date +%Y.%m.%d.%H.%M.%S)/ ../manifest.json > {{DIRNAME}}/manifest.json
    cp -r ../assets {{DIRNAME}}
    cp -r ../layouts {{DIRNAME}}
    zip -r {{FILENAME}} {{DIRNAME}}
